import random
import string

from turingarena import *

all_passed = True

def createNumber():
    lenght = random.randint(3, 10)
    number = ""
    for i in range(lenght):
        number += random.choice(list("123456789"))

    return int(number)

for _ in range(5):
    number = createNumber()
    anagram = list(str(number))
    random.shuffle(anagram)
    anagram = int(''.join(anagram))
    wrong_anagram = int(str(anagram)[1:])

    try:
        with run_algorithm(submission.source) as process:
            resultT = process.functions.is_anagram(anagram, number)
        with run_algorithm(submission.source) as process:
            resultF = process.functions.is_anagram(wrong_anagram, number)

        if (not resultF) and resultT:
            print("{0} is the anagram of {1} (correct!)".format(anagram, number))
            print("{0} isn't the anagram of {1} (correct!)".format(wrong_anagram, number))
        else:
            if not resultF:
                print("{0} isn't the anagram of {1} (wrong!)".format(anagram, number))
            if resultF:
                print("{0} is the anagram of {1} (wrong!)".format(wrong_anagram, number))
            all_passed = False
            break
    except AlgorithmError as e:
        print("Error")
        all_passed = False

print()
evaluation.data(dict(goals=dict(correct=all_passed)))

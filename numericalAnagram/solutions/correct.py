def is_anagram(parola, anagramma):
    parola, anagramma = str(parola), str(anagramma)
    lettere = {}
    for char in parola:
        if char != " ":
            if not (char in lettere.keys()) :
                lettere.update({char: 1})
            else:
                lettere.update({char: lettere[char] + 1})

    lettere2 = {}
    for char in anagramma:
        if char != " ":
            if not (char in lettere2.keys()) :
                lettere2.update({char: 1})
            else:
                lettere2.update({char: lettere2[char] + 1})

    if lettere == lettere2:
        return 1
    else:
        return 0

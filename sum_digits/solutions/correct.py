def sum_digits(num):
    sum = 0
    for n in str(num):
        sum += int(n)

    return sum

import random
import time
from turingarena import *

all_passed = True

def createNumber():
    lenght = random.randint(3, 8)
    number = 0
    sum_of_digits = 0

    for i in range(lenght):
        digit = random.choice(list("123456789"))
        number += int(digit) * (10 ** i)
        sum_of_digits += int(digit)

    return int(number), sum_of_digits

for _ in range(10):
    number, result = createNumber()

    try:
        with run_algorithm(submission.source) as process:
            sum = process.functions.sum_digits(number)

        if sum == result:
            print("{} digits sum --> {} (correct)".format(number, sum))
        else:
            print("{} digits sum --> {} (wrong)".format(number, sum))
            all_passed = False
    except AlgorithmError as e:
        print("Error")
        all_passed = False

evaluation.data(dict(goals=dict(correct=all_passed)))

import random
import math

from turingarena import *

def inFibonacciSequence(n):
    n = n * n * 5
    return (math.sqrt(n - 4) % 1 == 0) or (math.sqrt(n + 4) % 1 == 0)

all_passed = True

for _ in range(10):
    index = random.randint(0, 100)

    try:
        with run_algorithm(submission.source) as process:
            number = process.functions.fibonacci_seq(index)

        if inFibonacciSequence(number):
            print(f"{number} is in the Fibonacci's Sequence (correct!)")
        else:
            print(f"{number} is in the Fibonacci's Sequence (wrong!)")
            all_passed = False
            break
    except AlgorithmError as e:
        print("Error")
        all_passed = False

print()
evaluation.data(dict(goals=dict(correct=all_passed)))

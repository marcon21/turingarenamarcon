def fibonacci_seq(num):
    first = 1
    temp = 1
    second = 1

    for i in range(num - 2):
        temp = second
        second = first
        first += temp


    return first

def flip(num):
    flipped_num = 0
    len_num = len(str(num)) - 1

    for i in range(len_num + 1):
        multiplier = 10 ** (len_num - i)
        flipped_num += num // multiplier * (10 ** i)
        num = num % multiplier

    return flipped_num

import random

from turingarena import *

def check_if_flipped(a, b):
    return str(int(str(a)[::-1])) == str(b)


all_passed = True

for _ in range(10):
    a = random.randint(0, 10 ** 7)

    try:
        with run_algorithm(submission.source) as process:
            b = process.functions.flip(a)

        if check_if_flipped(a, b):
            print(a, "-->", b, "(correct!)", sep='\t')
        else:
            print(a, "-->", b, "(wrong!)", sep='\t')
            all_passed = False
    except AlgorithmError as e:
        print("Error")
        all_passed = False

evaluation.data(dict(goals=dict(correct=all_passed)))

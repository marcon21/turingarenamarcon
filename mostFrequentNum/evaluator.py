import random
import string

from turingarena import *

all_passed = True

def createNumber(n):
    lenght = random.randint(5, 20)
    number = ""
    for i in range(lenght):
        if not i % 2:
            number += n
        else:
            number += random.choice(list("0123456789"))

    return int(number)

for _ in range(10):
    n = random.choice(list("0123456789"))
    number = createNumber(n)

    try:
        with run_algorithm(submission.source) as process:
            frequentN = process.functions.mostFrequentN(number)

        if frequentN == int(n):
            print("{0} is the most frequent n in {1} (correct!)".format(frequentN, number))
        else:
            print("{0} isn't the most frequent n in {1} (wrong!)".format(frequentN, number))
            all_passed = False
            break
    except AlgorithmError as e:
        print("Error")
        all_passed = False

print()
evaluation.data(dict(goals=dict(correct=all_passed)))
